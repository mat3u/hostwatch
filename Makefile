# HostWatch
# Matt Stasch <matt.stasch@gmail.com>

CC=gcc
LFLAGS = $(shell pkg-config --libs libnotify)

ifeq ($(mode), release)
	CFLAGS =-std=c99 -O2 -Wall
else
	CFLAGS =-std=c99 -DDEBUG -g -O0 -Wall
endif

ifeq ($(c), strict)
	CFLAGS+=-Werror
endif

CFLAGS+= $(shell pkg-config --cflags libnotify)

OBJS=hostwatch.o config.o services.o notify.o misc.o

hostwatch: $(OBJS)
	$(CC) $^ $(LFLAGS) -o $@
	
$(OBJS): %.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o
	rm -f hostwatch
	rm -f core
	rm -f *.c~ *.h~
	
all: hostwatch

rebuild: clean all

doxygen:
	doxygen ./doxygen.conf

.PHONY: clean all doxygen rebuild
