/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#include <libnotify/notify.h>
#include <stdarg.h>
#include <stdlib.h>

#include "notify.h"
#include "misc.h"

/**
 * Nazwa instancji powiadomienia.
 */
#define INIT_NAME "HostWatch"

/**
 * Struktura opisująca powiadomienie.
 */
NotifyNotification *notification = NULL;

/**
 * Inicializuje mechanizm powiadomień.
 */
void notifyInit(void) {
    notify_init(INIT_NAME);
}

/**
 * Wyświetla powiadomienie.
 *
 * @param header Treść nagłówka powiadomienia.
 * @param message Tekst wiadomości do sformatowania.
 */
void notifyShow(const char *header, const char *message, ...) {
	char msg[256];

	va_list args;
	va_start(args, message);

	vsprintf(msg, message, args);

	if(notification == NULL)
		notification = notify_notification_new(header, msg, "dialog-warning");
	else
		notify_notification_update(notification, header, msg, "dialog-warning");

	DBG(msg);

	notify_notification_show(notification, NULL);
	
	va_end(args);
}

/**
 * Sprząta po mechanizmie powiadomień.
 */
void notifyFree(void) {
    notify_uninit();
}
