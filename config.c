/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>

#include "config.h"
#include "misc.h"

/**
 * Domyślny maksymalny czas operacji
 */
#define DEFAULT_TIMEOUT 	30

/**
 * Domyślna przerwa między kolejnymi sesjami sprawdzania dostępności
 */
#define DEFAULT_TIMESPAN	600

/**
 * Domyślna wartość flagi "gadatliwość"
 */
#define DEFULAT_VERBOSE 	0

/**
 * Domyślna wartość flagi działania w tle
 */
#define DEFULAT_DAEMON		1

/**
 * Domyślna ścieżka do pliku z konfiguracją
 */
#define DEFAULT_HOSTSFILE	"./hosts.cfg"

/**
 * Inicjalizuje domyślnymi wartościami zmienną przechowującą konfigurację.
 *
 * Funkcja sama zaalokuje pamięć i ustawi domyślne wartości.
 *
 * @param cfg Struktura opisująca konfigurację programu.
 */
void initConfig(Config ** cfg)
{
	*cfg = calloc(sizeof(Config), 1);

	(*cfg)->Timeout = DEFAULT_TIMEOUT;
	(*cfg)->Timespan = DEFAULT_TIMESPAN;
	(*cfg)->Verbose = DEFULAT_VERBOSE;
	(*cfg)->Daemon = DEFULAT_DAEMON;
	(*cfg)->HostsFile = NULL;
}

/**
 * Przetwarzanie argumentów wejściowych programu.
 *
 * @param ac Ilość parametrów wejściowych programu.
 * @param av Tablica stringów z parametrami wejściowymi.
 * @param cfg Struktura opisująca konfigurację programu.
 *
 * @return 0 jeżeli wszystko OK, -1 w razie błędu.
 */
int parseArgs(int ac, char **av, Config * cfg)
{
	int n = 0;
	long t = 0;

	for (n = 1; n < ac; n++) {
		switch ((int)av[n][0]) {
		case '-':
		case '/':
			switch (av[n][1]) {
				// Verbose mode
			case 'v':
			case 'V':
				cfg->Verbose = 1;
				break;
			case 'p':
			case 'P':
				cfg->Daemon = 0;
				break;
				// Timeout
			case 't':
			case 'T':
				if (n == ac - 1)
					return -1;
				t = atol(av[n + 1]);
				if (t > 60 || t < 1) {
					ERR("Timeout value out of range! Range: 1-60.\n");
					return -1;
				}

				cfg->Timeout = t;
				n++;
				break;
			case 'd':
			case 'D':
				if (n == ac - 1)
					return -1;
				t = atol(av[n + 1]);
				if (t < 1) {
					ERR("Timespan value must be larger than 1.\n");
					return -1;
				}

				cfg->Timespan = t;
				n++;
				break;
				// Configuration file
			case 'c':
			case 'C':
				if (n == ac - 1)
					return -1;
				cfg->HostsFile = (char *)strdup((const char*)av[n + 1]);
				n++;
				break;
			}
			break;
		default:
			return -1;
		}
	}

	if (cfg->HostsFile == NULL) {
		cfg->HostsFile = (char *)strdup((const char*)DEFAULT_HOSTSFILE);
	}

	return OK;
}

/**
 * Przetwarzanie tokenu opisującego pojedyńczą usługę.
 * 
 * @param svc Wskaźnik na strukturę opisującą usługę
 * @param token Pojedyńczy token do analizy
 *
 * @return 0 jeżeli wszystko OK, 1 jeżeli coś poszło nie tak
 */
int parseService(Service* svc, char* token)
{
	int port = atoi(token);

	if(port != 0)
	{
		svc->Proto = NULL;
		svc->Name = strdup(token);
		svc->Port = port;

		return 0;
	}

	struct servent* se = getservbyname(token, NULL);

	if(se == NULL) 
		return 1;

	svc->Proto = strdup(se->s_proto);
	svc->Name = strdup(se->s_name);
	svc->Port = ntohs(se->s_port);

	return 0;
}

/**
 * Przetwarza plik konfiguracyjny.
 *
 * @param cfg Struktura opisująca konfigurację programu.
 *
 * @return 0 jeżeli wszystko OK, -1 w razie błędu.
 */
int parseConfig(Config * cfg)
{
	char c, *line = (char *)calloc(sizeof(char), 256);
	long lpos = 0, lmax = 256, hmax = 4, hcurr = 0;

	FILE *f = NULL;

	if (!(f = fopen(cfg->HostsFile, "r"))) {
		ERR("Unable to open hosts file!");
		return -1;
	}

	cfg->Hosts = (Host *) calloc(sizeof(Host), hmax);
	cfg->HostCount = 0;

	while (!feof(f)) {
		c = fgetc(f);

		line[lpos++] = c;

		if (lpos == lmax) {
			lmax *= 2;
			line = (char *)realloc(line, sizeof(char) * lmax);
		}

		if (c != '\n' && !feof(f))
			continue;

		if(feof(f)) {
			line[lpos-1] = ' '; line[lpos+1] = 0;
		}

		line[lpos-1] = (char)0;

		DBG("CONFIG: Line -->%s<--\n", line);

		// Parsing
		{
			lmax = 4;
			lpos = 0;

			char *token = strtok(line, " \t");

			// Skip line if empty
			if(token == NULL) {
				DBG("CONFIG: Line is empty!\n");
				continue;
			}

			// Skip line if comment
			if (token[0] != '#') {

				Host *host = (Host *)malloc(sizeof(Host));

				host->Services = (Service*)calloc(sizeof(Service), lmax);
				host->Count = 0;

				while (token) {
					switch (lpos++) {
					
					// Host address on first position
					case 0:
						host->Address = (char *)strdup((const char*)token);
						break;
					default:
						if(parseService(&(host->Services[host->Count]), token) == 0) {
							host->Count++;

							// Expand available space for services if needed
							if (host->Count == lmax) {
								lmax *= 2;
								host->Services = (Service*) realloc(host->Services, sizeof(Service) * lmax);
							}
						} else {
							ERR("Service configuration parsing error! Unknown value: \"%s\". Value ommited.\n", token);
						}
					}

					DBG("CONFIG: Token -->%s<--\n", token);
					token = strtok(NULL, " \t");
				}

				memcpy(&cfg->Hosts[hcurr++], host, sizeof(Host));
				cfg->HostCount++;
				free(host);

				// Expand available space for hosts if needed
				if (hcurr == hmax) {
					hmax *= 2;
					cfg->Hosts = (Host *) realloc(cfg->Hosts, sizeof(Host) * hmax);
				}
			} else {
				DBG("CONFIG: Comment\n");
			}

		}

		free(line);
		line = NULL;
		line = (char*)calloc(sizeof(char), 256);
		lmax = 256;
		lpos = 0;
	}

	free(line);
	line = NULL;

	fclose(f);

	return OK;
}

/**
 * Sprząta po strukturze konfiguracyjnej.
 * 
 * @param cfg Struktura opisująca konfigurację programu.
 */
void freeConfig(Config ** cfg)
{
	int i = 0, j = 0;

    if(*cfg == NULL)
        return;

	Config *c = (*cfg);

	free(c->HostsFile);

	if (c->Hosts != NULL) {
		for (i = 0; i < c->HostCount; i++) {
			free(c->Hosts[i].Address);
			
			// Free services details
			for(j = 0; j < c->Hosts[i].Count; j++)
			{
				free(c->Hosts[i].Services[j].Name);
				free(c->Hosts[i].Services[j].Proto);
			}
			
			free(c->Hosts[i].Services);
		}

		free(c->Hosts);
	}

	free(c);
	
	*cfg = NULL;
}
