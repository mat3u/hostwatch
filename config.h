/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdint.h>

/**
 * Opisuje pojedyńczą usługę
 */
struct s_Service {
	char* Name; 					/*!< Nazwa która będzie wyświetlana w komunikatach. */
	int Port;						/*!< Port związany z usługą. */
	char* Proto;					/*!< Protokół związany z usługą. */
};

/**
 * Opisuje pojedyńczego host-a.
 */
struct s_Host {
	char *Address;					/*!< Adres hosta lub adres IP */
	struct s_Service *Services;		/*!< Tablica usług, które mają być monitorowane */
	uint16_t Count;					/*!< Ilość usług w tablicy. */
};

/**
 * Opisuje konfigurację programu.
 */
struct s_Config {
	uint8_t Verbose:1;				/*!< Flaga "gadatliwości". */
	uint8_t Daemon:1;				/*!< Flaga działania w tle. */
	uint8_t Timeout:6;				/*!< Maks. czas pojedyńczej operacji */
	uint16_t Timespan;				/*!< Przerwa między sesjami monitorowania */

	char *HostsFile;				/*!< Ścieżka do pliku z konfiguracją */

	struct s_Host *Hosts;			/*!< Tablica z opisami hostów do zbadania */
	int HostCount;					/*!< Ilość elementów w tablicy. */
};

/**
 * Opisuje konfigurację programu.
 */
typedef struct s_Config Config;

/**
 * Opisuje pojedyńczego host-a.
 */
typedef struct s_Host Host;

/**
 * Opisuje pojedyńczą usługę
 */
typedef struct s_Service Service;

/**
 * Inicjalizuje domyślnymi wartościami zmienną przechowującą konfigurację.
 *
 * Funkcja sama zaalokuje pamięć i ustawi domyślne wartości.
 *
 * @param cfg Struktura opisująca konfigurację programu.
 */
void initConfig(Config **);

/**
 * Sprząta po strukturze konfiguracyjnej.
 * 
 * @param cfg Struktura opisująca konfigurację programu.
 */
void freeConfig(Config **);

/**
 * Przetwarza plik konfiguracyjny.
 *
 * @param cfg Struktura opisująca konfigurację programu.
 *
 * @return 0 jeżeli wszystko OK, -1 w razie błędu.
 */
int parseConfig(Config *);

/**
 * Przetwarzanie argumentów wejściowych programu.
 *
 * @param ac Ilość parametrów wejściowych programu.
 * @param av Tablica stringów z parametrami wejściowymi.
 * @param cfg Struktura opisująca konfigurację programu.
 *
 * @return 0 jeżeli wszystko OK, -1 w razie błędu.
 */
int parseArgs(int, char **, Config *);

#endif
