/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#ifndef _NOTIFY_H_
#define _NOTIFY_H_

/**
 * Inicializuje mechanizm powiadomień.
 */
void notifyInit(void);

/**
 * Wyświetla powiadomienie.
 *
 * @param header Treść nagłówka powiadomienia.
 * @param message Tekst wiadomości do sformatowania.
 */
void notifyShow(const char *header, const char *message, ...);

/**
 * Sprząta po mechanizmie powiadomień.
 */
void notifyFree(void);

#endif
