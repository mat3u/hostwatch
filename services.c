/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/select.h>
#include <errno.h>
#include <arpa/inet.h>

#include "config.h"
#include "services.h"
#include "misc.h"

/**
 * \enum Określa typ protokołu
 */
enum Proto {
	PROTO_TCP = 1,
	PROTO_UDP = 2
};

/**
 * Wypełnia strukturę sockaddr_in dla podanego adresu i portu.
 * 
 * @param addr Adres host-a lub jego IP
 * @param port Numer portu
 * @param sin Struktura sockaddr_in.
 * @return 0 jeżeli OK, w przeciwnym razie kod błędu.
 */
int str2addr(const char* addr, int port, struct sockaddr_in *sin) {
	struct hostent *host;

	if((host = gethostbyname(addr)) != NULL) {
		sin->sin_port = htons(port);
		sin->sin_family = AF_INET;
		memcpy(&(sin->sin_addr), host->h_addr_list[0], host->h_length);
	} else {
		if(errno == ETIMEDOUT)
			return HW_EUNABLETORESOLVE;

		return errno;
	}

	return OK;
}

/**
 * Sprawdza czy usługa jest aktywna.
 *
 * @param addr Adres host-a lub jego IP
 * @param service Numer portu
 * @param timeout Maks. czas operacji.
 * @param proto Protokół (TCP lub UDP)
 * @return 0 jeżeli OK, w przeciwnym razie kod błędu.
 */
int checkServiceOnPort(const char* addr, int service, uint16_t timeout, enum Proto proto) {
	struct sockaddr_in sin;
	struct timeval tv;
	int err = OK, sockfd, flags;
	socklen_t len = sizeof(err);
	fd_set wset;

	if((err = str2addr(addr, service, &sin)) != 0) {
		return err;
	}

	int socktype = proto == PROTO_TCP ? SOCK_STREAM : SOCK_DGRAM;

	sockfd = socket(AF_INET, socktype, OK);

	flags = fcntl(sockfd, F_GETFL, 0);
			fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

	if(!connect(sockfd, (struct sockaddr *)&sin, sizeof(sin))) {
		close(sockfd);
		return OK;
	}

	if(errno != EINPROGRESS){
		close(sockfd);
		return errno;
	}

	FD_ZERO(&wset);
	FD_SET(sockfd, &wset);

	tv.tv_usec = 0;
	tv.tv_sec = timeout;

	switch(select(FD_SETSIZE, 0, &wset, 0, &tv))
	{
		case 0:
			return ETIMEDOUT;
		case 1:
			getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &err, &len);
			if (err == 0) {
				close(sockfd);
				return OK;;
			}
			return err;
		default:
			return errno;
	}
}

/**
 * Sprawdza czy usługa jest online.
 *
 * @param addr Adres host-a lub jego IP.
 * @param service Struktura opisująca usługę.
 * @param timeout Maks. czas operacji.
 * @return 0 jeżeli OK, w przeciwnym razie kod błędu.
 */
int checkService(const char* addr, Service* service, uint16_t timeout)
{
	DBG("Address: %s\nService: %s:%d:%s\nTimeout: %d\n", 
		addr, 
		service->Name, 
		service->Port, 
		service->Proto,
		timeout);

	int proto = PROTO_UDP;
	
	if(service->Proto == NULL || strcmp(service->Proto, "tcp") == 0)
		proto = PROTO_TCP;

	return checkServiceOnPort(addr, service->Port, timeout, proto);
}

/**
 * Sprawdza czy host jest aktywny.
 * 
 * Próbuje nawiązać połączenie na porcie 80/tcp
 * jeżeli nawiąże połączenie lub host go aktywnie odmówi
 * uznaje, że host jest aktywny.
 * 
 * @param addr Adres host-a lub jego IP.
 * @param timeout Maks. czas operacji.
 * @return 0 jeżeli OK, w przeciwnym razie kod błędu.
 */
int checkHost(const char *addr, uint16_t timeout) {
	int err = checkServiceOnPort(addr, 80, timeout, PROTO_TCP);

	// Assumption: If remote machine refuses connection is online
	if(err == ECONNREFUSED || err == OK) return OK;

	return err;
}

