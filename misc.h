/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */
 
#ifndef _MISC_H_
#define _MISC_H_

/**
 * Makro do wypisywanie błędów.
 *
 * Wyprowadza informacje na STDERR.
 */
#define ERR(...) fprintf(stderr, __VA_ARGS__)

/**
 * Makro do wypisywania informacji.
 *
 * Wyprowadza informacje na STDOUT.
 */
#define LOG(...) fprintf(stdout, __VA_ARGS__)

/**
 * Makro do wypisywania informacji pomocniczych.
 *
 * Wyprowadza informacje na STDOUT.
 */
#ifdef DEBUG
#define DBG(...) fprintf(stdout, __VA_ARGS__)
#else
#define DBG(...) ;
#endif

/**
 * Kod opisujący powodzenie operacji.
 */ 
#ifndef OK
#define OK 0
#endif

/**
 * Własny kod błędu - opisujący fakt że nie udało się znaleźć adresu IP dla host-a.
 */
#define HW_EUNABLETORESOLVE 1025 // 1024 & 1

/**
 * Zewnętrzna funkcja do klonowania napisów.
 *
 * @param str Wejściowy ciąg znaków.
 * 
 * @return Kopia podanego ciągu znaków.
 */
extern char* strdup(const char* str);

/**
 * Zwraca komunikat opisujący podany błąd.
 *
 * Rozszerza funkcjonalność funkcji *strerror* o własne kody błędów.
 * 
 * @param err Kod błędu.
 *
 * @return Komunikat opisujący błąd.
 */
const char* h_errstr(int err);

#endif
