/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#ifndef _SERVICES_H_
#define _SERVICES_H_

/**
 * Sprawdza czy host jest aktywny.
 * 
 * Próbuje nawiązać połączenie na porcie 80/tcp
 * jeżeli nawiąże połączenie lub host go aktywnie odmówi
 * uznaje, że host jest aktywny.
 * 
 * @param addr Adres host-a lub jego IP.
 * @param timeout Maks. czas operacji.
 * @return 0 jeżeli OK, w przeciwnym razie kod błędu.
 */
int checkHost(const char *addr, uint16_t timeout);

/**
 * Sprawdza czy usługa jest online.
 *
 * @param addr Adres host-a lub jest IP.
 * @param service Struktura opisująca usługę.
 * @param timeout Maks. czas operacji.
 * @return 0 jeżeli OK, w przeciwnym razie kod błędu.
 */
int checkService(const char *addr, Service* service, uint16_t timeout);

#endif
