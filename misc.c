/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#include <string.h>

#include "misc.h"

/**
 * Zwraca komunikat opisujący podany błąd.
 *
 * Rozszerza funkcjonalność funkcji *strerror* o własne kody błędów.
 * 
 * @param err Kod błędu.
 *
 * @return Komunikat opisujący błąd.
 */
const char* h_errstr(int err) {
	switch(err)
	{
		case HW_EUNABLETORESOLVE:
			return "Unable to resolve hostname!";
		default:
			return (const char*)strerror(err);
	}
}
