/**
 * @file
 * 
 * \author Matt Stasch <matt.stasch@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include "config.h"
#include "misc.h"
#include "notify.h"
#include "services.h"

/**
 * Wiadomość o sposobie użycia programu.
 */
#define USAGE "Host Watch 0.1\n\t%s [-v] [-p] [-t NUMBER] [-c FILE]\n\n \
\t-v\tVerbose mode\n \
\t-p\tRun as application\n \
\t-t\tService check timeout\n \
\t-c\tHosts configuration\n\n \
Matt Stasch <matt.stasch@gmail.com>\n"

/**
 * Konfiguracja systemu.
 */
Config *config = NULL;

/**
 * Kod zwracany przez aplikację.
 */
int retcode = 0;

/**
 * Wykonuje operacje niezbędne przed zamknięciem programu.
 */
void atExit(void);

/**
 * Reaguje na sygnały docierające do aplikacji.
 *
 * @param signum Kod sygnału.
 */
void atSignal(int signum);

/**
 * Punkt startowy programu.
 *
 * @param ac Ilość parametrów wejściowych programu.
 * @param av Tablica stringów z parametrami wejściowymi.
 *
 * @return Kod wyjścia aplikacji.
 */
int main(int ac, char **av)
{
	atexit(atExit);

	initConfig(&config);

	if (parseArgs(ac, av, config) == -1) {
		fprintf(stderr, USAGE, av[0]);
		return -1;
	}

	if (config->Verbose) {
		LOG("Configuration\n");
		LOG(" * Timeout:\t\t %d\n", config->Timeout);
		LOG(" * Cfg file:\t\t %s\n", config->HostsFile);
		LOG(" * Daemon mode:\t\t %d\n", config->Daemon);
	}

	if (parseConfig(config) == -1) {
		ERR("Unable to parse configuration file!");
		return -1;
	}

	if (config->HostCount == 0) {
		LOG("Nothing to do!\n");
		return 0;
	}

	signal(SIGQUIT, atSignal);
	signal(SIGINT, atSignal);
	
	notifyInit();

	// Deamonize application
	if (config->Daemon) {
		switch (fork()) {
			case -1:
				ERR("Unable to daemonize application!");
				return -1;
			case 0:
				if(config->Verbose)
					LOG("Fork! Pid: %d\n", getpid());
				fclose(stdin);
				fclose(stdout);
				fclose(stderr);
				break;
			default:
				exit(0);
		}
	}

	int hostN = 0, err = 0;

	// Check if hosts & services are alive
	while (retcode == 0) {
		// Get next host
		Host *h = &config->Hosts[hostN];

		// Check if host is accessible
		if ((err = checkHost(h->Address, config->Timeout)) != 0) {
			DBG("HOST: %s - [%d] %s\n",h->Address, err, h_errstr(err));
			notifyShow("HostWatch", "Your host %s seems to be unaccessible!\nError message: %s\n", 
			           h->Address, 
			           h_errstr(err));
		} else if (h->Count > 0) {
		    DBG("HOST OK: %s - [%d] %s\n",h->Address, err, h_errstr(err));
		
		    // Check if services are online
		    for (int v = 0; v < h->Count; v++) {
			    if ((err = checkService(h->Address, &(h->Services[v]), config->Timeout)) != 0) {
				    DBG("SVC: %s:%d - [%d] %s\n",h->Address, h->Services[v].Port, err, h_errstr(err));
				    notifyShow("HostWatch", "Service '%s' on %s is not available!\nError message: %s\n",
				               h->Services[v].Name, 
				               h->Address, 
				               h_errstr(err));
			    } else {
			        DBG("SVC OK: %s:%d - [%d] %s\n",h->Address, h->Services[v].Port, err, h_errstr(err));
			    }
		    }
		}
		
		if (++hostN == config->HostCount) {
			sleep(config->Timespan);
			hostN = 0;
		}
		else {
			sleep(10);
		}
	}

	return retcode;
}

/**
 * Reaguje na sygnały docierające do aplikacji.
 *
 * @param signum Kod sygnału.
 */
void atSignal(int signum)
{
	retcode = signum;
	
	if (config == NULL || config->Verbose) {
		ERR("Signal: %d!\n", signum);
	}
}

/**
 * Wykonuje operacje niezbędne przed zamknięciem programu.
 */
void atExit(void) {
	DBG("Exiting program...!");	
	notifyFree();
	
	if(config != NULL)
		freeConfig(&config);
}
